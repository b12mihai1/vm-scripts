#!/bin/bash

set -e

VM_NAME='pm-vm-physical'
IMG_OUTPUT_PATH='pm-vm-physical-1.0.img'
PART_OUTPUT_PATH='pm-vm-physical-1.0.ext4'

# Extract the IMG from the VM
vboxmanage clonemedium disk ~/VirtualBox\ VMs/${VM_NAME}/ubuntu-cosmic-18.10-cloudimg.vmdk --format RAW ${IMG_OUTPUT_PATH}

# Remove the IMG from VirtualBox list of known media
vboxmanage closemedium disk ${IMG_OUTPUT_PATH}

# Get the offset of the partition inside the hdd-image and extract it
dd if=${IMG_OUTPUT_PATH} bs=512 skip=`fdisk -l ${IMG_OUTPUT_PATH} | tail -1 | tr -s ' ' | cut -d' ' -f3` of=${PART_OUTPUT_PATH}

# Check for defects
e2fsck -fp ${PART_OUTPUT_PATH}

# Shrink the partition
resize2fs -M ${PART_OUTPUT_PATH}

# Check for defects
e2fsck -fp ${PART_OUTPUT_PATH}

md5sum ${PART_OUTPUT_PATH}
sha1sum ${PART_OUTPUT_PATH}
